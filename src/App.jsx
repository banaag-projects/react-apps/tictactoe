import React, { useState } from "react";

const rowStyle = {
  display: "flex",
};

const squareStyle = {
  width: "60px",
  height: "60px",
  backgroundColor: "#ddd",
  margin: "4px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  fontSize: "20px",
  color: "white",
};

const boardStyle = {
  backgroundColor: "#eee",
  width: "208px",
  alignItems: "center",
  justifyContent: "center",
  display: "flex",
  flexDirection: "column",
  border: "3px #eee solid",
};

const containerStyle = {
  display: "flex",
  alignItems: "center",
  flexDirection: "column",
};

const instructionsStyle = {
  marginTop: "5px",
  marginBottom: "5px",
  fontWeight: "bold",
  fontSize: "16px",
};

const buttonStyle = {
  marginTop: "15px",
  marginBottom: "16px",
  width: "80px",
  height: "40px",
  backgroundColor: "#8acaca",
  color: "white",
  fontSize: "16px",
};

function Square({ value, onSquareClick }) {
  return (
    <button className="square" style={squareStyle} onClick={onSquareClick}>
      {value}
    </button>
  );
}

function Board() {
  const [squareValueArray, setSquareValueArray] = useState(Array(9).fill(null));
  const [isXplayer, setIsXplayer] = useState(true);

  function handleClick(i) {
    if (checkWinner(squareValueArray)) {
      return; //ends the function immediately if there is a return in checkWinner
    }
    if (squareValueArray[i] !== null) {
      return;
    }
    const newSquareValueArray = [...squareValueArray];

    if (isXplayer) {
      newSquareValueArray[i] = "X";
    } else {
      newSquareValueArray[i] = "O";
    }

    setSquareValueArray(newSquareValueArray);
    setIsXplayer(!isXplayer);
    console.log(squareValueArray);
  }

  function handleReset() {
    var squareValueArrayReset = [...squareValueArray];
    squareValueArrayReset = Array(9).fill(null);
    setSquareValueArray(squareValueArrayReset);
    setIsXplayer(true);
    console.log(squareValueArray);
  }

  const winner = checkWinner(squareValueArray);
  return (
    <div style={containerStyle} className="gameBoard">
      <div id="statusArea" className="status" style={instructionsStyle}>
        Next player: <span>{isXplayer ? "X" : "O"}</span>
      </div>
      <div id="winnerArea" className="winner" style={instructionsStyle}>
        Winner: <span>{winner !== null ? winner : "None"}</span>
      </div>
      <button style={buttonStyle} onClick={handleReset}>
        Reset
      </button>
      <div style={boardStyle}>
        <div className="board-row" style={rowStyle}>
          <Square
            value={squareValueArray[0]}
            onSquareClick={() => handleClick(0)}
          />
          <Square
            value={squareValueArray[1]}
            onSquareClick={() => handleClick(1)}
          />
          <Square
            value={squareValueArray[2]}
            onSquareClick={() => handleClick(2)}
          />
        </div>
        <div className="board-row" style={rowStyle}>
          <Square
            value={squareValueArray[3]}
            onSquareClick={() => handleClick(3)}
          />
          <Square
            value={squareValueArray[4]}
            onSquareClick={() => handleClick(4)}
          />
          <Square
            value={squareValueArray[5]}
            onSquareClick={() => handleClick(5)}
          />
        </div>
        <div className="board-row" style={rowStyle}>
          <Square
            value={squareValueArray[6]}
            onSquareClick={() => handleClick(6)}
          />
          <Square
            value={squareValueArray[7]}
            onSquareClick={() => handleClick(7)}
          />
          <Square
            value={squareValueArray[8]}
            onSquareClick={() => handleClick(8)}
          />
        </div>
      </div>
    </div>
  );
}

function checkWinner(squareValueArray) {
  const winningPatternsArray = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  for (let i = 0; i < winningPatternsArray.length; i++) {
    const [a, b, c] = winningPatternsArray[i];

    if (
      squareValueArray[a] &&
      squareValueArray[a] === squareValueArray[b] &&
      squareValueArray[a] === squareValueArray[c]
    ) {
      return squareValueArray[a];
    }
  }

  return null;
}

export default function Game() {
  return (
    <div className="game">
      <div className="game-board">
        <Board />
      </div>
    </div>
  );
}
